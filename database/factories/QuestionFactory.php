<?php
use App\Model;
use App\Question;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/** @var \Illuminate\Database\Eloquent\Factory $factory */



$factory->define(Question::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence(rand(5,10),"."),
        'body'=>$faker->paragraphs(rand(3,7),true),
        'views'=>rand(0,10),
        //'answers_count'=>rand(0,10),
        'votes'=>rand(-3,10)
    ];
});
